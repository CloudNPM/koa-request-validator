'use strict';

const co = require('co');
const _ = require('lodash');

class Field {
  /**
   *
   * @param {String} path
   * @param {String} [where='body']
   */
  constructor(path, where) {
    this._path = path;
    this._where = where || 'body';
    this._required = false;
    this._requiredMessage = 'Required';
    this._validators = new Map;
  }

  /**
   * set this field required
   * @param {String} [message='Required']
   * @returns {Field}
   */
  required(message) {
    this._required = true;
    this._requiredMessage = message || 'Required';
    return this;
  }

  /**
   * set this field optional
   * @returns {Field}
   */
  optional() {
    this._required = false;
    return this;
  }

  /**
   * add a validation rule
   * @param {Function} validator - returns true for valid or false for invalid
   * @param {String} errorMessage - the error message to show if the field failed this validation rule
   * @returns {Field}
   */
  validate(validator, errorMessage) {
    if ('function' !== typeof validator) {
      throw new Error('Validator must be a function');
    }
    if (this._validators.has(errorMessage)) {
      throw new Error('Duplicate error message');
    }
    if (!errorMessage) {
      throw new Error('Error message missing');
    }
    this._validators.set(errorMessage, validator);
    return this;
  }

  /**
   *
   * @param {koa.Context} ctx
   * @returns {Promise.<String||undefined>}
   * @private
   */
  _processRules(ctx) {
    return co.call(this, function*() {
      var val = _.get(_.get(ctx.request, this._where), this._path);
      if (val) {
        for (var errMsg of this._validators.keys()) {
          var validator = this._validators.get(errMsg);
          var result = yield Promise.resolve(validator.call(ctx, val));
          if (!result) {
            return errMsg;
          }
        }
      } else if (this._required) {
        return this._requiredMessage;
      }
    });
  }
}

module.exports = Field;