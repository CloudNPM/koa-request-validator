'use strict';

const Field = require('./field');
const _ = require('lodash');
const co = require('co');

class Validator {
  /**
   * Validator
   */
  constructor() {
    this._fields = [];
  }

  /**
   * Get a field
   * @param {String} path
   * @param {String} [where='body']
   * @returns {Field}
   */
  field(path, where) {
    where = where || 'body';
    for (var i = 0; i < this._fields.length; i++) {
      var f = this._fields[i];
      if (f._path === path && f._where === where) {
        return f;
      }
    }
    var field = new Field(path, where);
    this._fields.push(field);
    return field;
  }

  /**
   * Validate a request
   * @param {koa.Context} ctx - the koa context of the request to validate
   * @returns {Promise.<Object>} - the error object
   */
  validate(ctx) {
    return co.call(this, function*() {
      var errPromises = {};
      for (var i = 0; i < this._fields.length; i++) {
        var field = this._fields[i];
        errPromises[field._path] = field._processRules(ctx);
      }
      var errObj = yield errPromises;
      return _.pickBy(errObj, _.isString);
    });
  }

  /**
   * Get the middleware to use
   * @returns {Function}
   */
  middleware() {
    var validator = this;
    return function*(next) {
      var err = yield validator.validate(this);
      if (_.keys(err).length > 0) {
        this.body = err;
        this.status = 422;
      } else {
        yield next;
      }
    }
  }
}

module.exports = Validator;
