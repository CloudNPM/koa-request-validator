'use strict';

const chai = require('chai');
chai.use(require('chai-as-promised'));
const expect = chai.expect;
const Field = require('../lib/field');
const Validator = require('../lib/validator');
const koa = require('koa');
const rp = require('request-promise');
const http = require('http');

describe('Test', ()=> {
  describe('Field', ()=> {
    describe('Constructor', ()=> {
      it('should return an instance of the Field class', ()=> {
        var f = new Field();
        expect(f).to.be.an.instanceof(Field);
      });
      it('should init the necessary properties', ()=> {
        var f = new Field('path', 'where');
        expect(f._path).to.eq('path');
        expect(f._where).to.eq('where');
        expect(f._required).to.exist;
        expect(f._requiredMessage).to.exist;
        expect(f._validators).to.exist;
      });
      it('should set the "_where" field to "body" if it is missing in constructor call', ()=> {
        var f = new Field('path');
        expect(f._where).to.eq('body');
      });
    });
    describe('Instance Methods', ()=> {
      describe('required', ()=> {
        it('should set the field required', ()=> {
          var f = new Field();
          expect(f._required).to.be.false;
          f.required();
          expect(f._required).to.be.true;
        });
        it('should set the required error message', ()=> {
          var f = new Field();
          f.required('new required error message');
          expect(f._requiredMessage).to.eq('new required error message');
        });
        it('should set the required error message to "Required" if the argument is missing.', ()=> {
          var f = new Field();
          f.required('another message');
          expect(f._requiredMessage).to.eq('another message');
          f.required();
          expect(f._requiredMessage).to.eq('Required');
        });
        it('should return the field instance itself', ()=> {
          var f = new Field();
          expect(f.required()).to.eq(f);
        });
      });
      describe('optional', ()=> {
        it('should set the field optional', ()=> {
          var f = new Field();
          f.required();
          expect(f._required).to.be.true;
          f.optional();
          expect(f._required).to.be.false;
        });
        it('should return the field instance itself', ()=> {
          var f = new Field();
          expect(f.optional()).to.eq(f);
        });
      });
      describe('validate', ()=> {
        it('should add a validation rule to the field', ()=> {
          var f = new Field();
          expect(f._validators.size).to.eq(0);
          f.validate(val=>true, 'error message');
          expect(f._validators.size).to.eq(1);
        });
        it('should return the field instance itself', ()=> {
          var f = new Field();
          expect(f.validate(val=>true, 'error message')).to.eq(f);
        });
        it('should throw if the first argument is not a function', ()=> {
          var f = new Field();
          expect(()=> {
            // noinspection JSCheckFunctionSignatures
            f.validate('not a function', 'error message');
          }).to.throw('Validator must be a function');
        });
        it('should throw if the error message was already added', ()=> {
          var f = new Field();
          f.validate(val=>true, 'error');
          expect(()=> {
            f.validate(val=>true, 'error');
          }).to.throw('Duplicate error message');
        });
        it('should throw if the error message is missing', ()=> {
          var f = new Field();
          expect(()=> {
            f.validate(val=>true);
          }).to.throw('Error message missing');
        });
      });
      describe('_processRules', ()=> {
        var mockContext = {
          request: {
            body: {
              a: 1,
              dotted: {path: 1}
            },
            elsewhere: {
              a: 2
            }
          }
        };
        it('should return a promise', ()=> {
          var f = new Field('path');
          expect(f._processRules(mockContext)).to.be.an.instanceof(Promise);
        });
        it('should return a promise that resolves to the error message if the validation fails', ()=> {
          var f = new Field('a');
          f.validate(val=>false, 'error message');
          return expect(f._processRules(mockContext)).to.eventually.eq('error message');
        });
        it('should return a promise that resolves to undefined if the validation passes', ()=> {
          var f = new Field('a');
          f.validate(val=>true, 'error message');
          return expect(f._processRules(mockContext)).to.eventually.be.undefined;
        });
        it('should be ok for the validators to return promises', ()=> {
          var f = new Field('a');
          f.validate(val=>Promise.resolve(true), 'err1')
              .validate(val=>Promise.resolve(false), 'err2');
          return expect(f._processRules(mockContext)).to.eventually.eq('err2');
        });
        it('should ran the validators in order and stops on the first failed rule', ()=> {
          var f = new Field('a');
          var validatorRanCount1 = 0;
          var validatorRanCount2 = 0;
          var validatorRanCount3 = 0;
          f
              .validate(()=> {
                validatorRanCount1++;
                return true;
              }, 'err1')
              .validate(()=> {
                validatorRanCount2++;
                return false;
              }, 'err2')
              .validate(()=> {
                validatorRanCount3++;
                return false;
              }, 'err3');
          f._processRules(mockContext)
              .then(err=> {
                expect(err).to.eq('err2');
                expect(validatorRanCount1).to.eq(1);
                expect(validatorRanCount2).to.eq(1);
                expect(validatorRanCount3).to.eq(0);
              });
        });
        it('should return a promise that rejects if a validator throws an error when called', ()=> {
          var f = new Field('a');
          f.validate(()=> {
            throw new Error('error thrown')
          }, 'err msg');
          return expect(f._processRules(mockContext)).to.eventually.be.rejectedWith('error thrown');
        });
        it('should return a promise that rejects if the validator returns a rejected promise', ()=> {
          var f = new Field('a');
          f.validate(val=> Promise.reject(new Error('rejected promise')), 'err msg');
          return expect(f._processRules(mockContext)).to.eventually.be.rejectedWith('rejected promise');
        });
        it('should be ok to use dotted paths', ()=> {
          var f = new Field('dotted.path');
          f.validate(val=>val === 2, 'err');
          return expect(f._processRules(mockContext)).to.eventually.eq('err');
        });
        it('should look for the value to validate in the object specified by the "where" property', ()=> {
          var f = new Field('a', 'elsewhere');
          f.validate(val=>val !== 2, 'err');
          return expect(f._processRules(mockContext)).to.eventually.eq('err');
        });
        it('should fail the validation if the field is required but not present in the request', ()=> {
          var f = new Field('b');
          f.required();
          return expect(f._processRules(mockContext)).to.eventually.eq('Required');
        });
        it('should return a promise that resolves to the user defined error message if it was changed', ()=> {
          var f = new Field('b');
          f.required('another required error message');
          return expect(f._processRules(mockContext)).to.eventually.eq('another required error message');
        });
        it('should not fail the validation if the field is optional and the value is missing from the request, ' +
            'even if null/undefined would fail the validator call', ()=> {
          var f = new Field('b');
          f.optional()
              .validate(val=>false, 'err');
          return expect(f._processRules(mockContext)).to.eventually.be.undefined;
        });
        it('should not run the validators if the field is optional and the value is missing from the request', ()=> {
          var f = new Field('b');
          var ranCount = 0;
          f.optional()
              .validate(()=> {
                ranCount++;
                return false
              }, 'err');
          return f._processRules(mockContext)
              .then(err=> {
                expect(err).to.not.exist;
                expect(ranCount).to.eq(0);
              });
        });
      });
    });
  });
  describe('Validator', ()=> {
    describe('Constructor', ()=> {
      it('should return an instance of the Validator class', ()=> {
        var v = new Validator();
        expect(v).to.be.an.instanceof(Validator);
      });
    });
    describe('Instance Methods', ()=> {
      describe('field', ()=> {
        it('should return an instance of the Field class', ()=> {
          var v = new Validator();
          var field = v.field('path');
          expect(field).to.be.an.instanceof(Field);
        });
        it('should store and return a new field instance if path and where combination was not already there', ()=> {
          var v = new Validator();
          var f1 = v.field('path1', 'where1');
          var f2 = v.field('path2', 'where1');
          var f3 = v.field('path2', 'where2');
          expect(f1).to.not.eq(f2);
          expect(f2).to.not.eq(f3);
          expect(f1).to.not.eq(f3);
        });
        it('should return the same field instance if path and where combination was already there', ()=> {
          var v = new Validator();
          var f1 = v.field('path', 'where');
          var f2 = v.field('path', 'where');
          expect(f1).to.eq(f2);
        });
        it('should set the where arg to "body" if omitted', ()=> {
          var v = new Validator();
          expect(v.field('path')).to.eq(v.field('path', 'body'));
        });
      });
      describe('validate', ()=> {
        var mockContext = {
          request: {
            body: {
              a: 1,
              b: 2
            }
          }
        };
        it('should return a promise that resolves to an empty object if the request is valid', ()=> {
          var v = new Validator();
          v.field('a').validate(val=>val === 1, 'err a');
          v.field('b').validate(val=>val === 2, 'err b');
          return expect(v.validate(mockContext)).to.eventually.deep.equal({});
        });
        it('should return a promise that resolves to an error object if the request is not valid', ()=> {
          var v = new Validator();
          v.field('a').validate(val=>val !== 1, 'err a');
          v.field('b').validate(val=>val !== 2, 'err b');
          return expect(v.validate(mockContext)).to.eventually.deep.equal({a: 'err a', b: 'err b'});
        });
        it('should only contain the fields that failed the validation in the error object', ()=> {
          var v = new Validator();
          v.field('a').validate(val=>val !== 1, 'err a');
          v.field('b').validate(val=>val === 2, 'err b');
          return expect(v.validate(mockContext)).to.eventually.deep.equal({a: 'err a'});
        });
        it('should only have the first error message if multiple rules were not met', ()=> {
          var v = new Validator();
          v.field('a')
              .validate(val=>val !== 1, 'err a1')
              .validate(val=>val !== 1, 'err a2')
              .validate(val=>val !== 1, 'err a3');
          v.field('b').validate(val=>val === 2, 'err b');
          return expect(v.validate(mockContext)).to.eventually.deep.equal({a: 'err a1'});
        });
        it('should process the rules in order and breaks on the first unmet rule', ()=> {
          var v = new Validator();
          var count1 = 0;
          var count2 = 0;
          var count3 = 0;
          v.field('a')
              .validate(val=> {
                count1++;
                return val === 1;
              }, 'err1')
              .validate(val=> {
                count2++;
                return val !== 1;
              }, 'err2')
              .validate(val=> {
                count3++;
                return val !== 1;
              }, 'err3');
          return v.validate(mockContext)
              .then(err=> {
                expect(err).to.deep.equal({a: 'err2'});
                expect(count1).to.eq(1);
                expect(count2).to.eq(1);
                expect(count3).to.eq(0);
              });
        });
      });
      describe('middleware', ()=> {
        var server;
        var app = koa();
        var router = require('koa-router')();

        var v1 = new Validator();
        v1.field('val', 'header').required().validate(val=>val === 'valid', 'invalid');
        router.get('/', v1.middleware(), function*() {
          this.body = {reachedHere: true};
        });

        var v2 = new Validator();
        v2.field('val1', 'header').required();
        v2.field('val2', 'header').required().validate(function(val2) {
          return val2 !== this.request.header.val1;
        }, 'err');
        router.get('/this_arg', v2.middleware(), function*() {
          this.body = {reachedHere: true};
        });

        app.use(router.routes());
        app.use(router.allowedMethods());

        before(done=> {
          server = app.listen(3000, done);
        });
        after(done=> {
          server.close(done);
        });

        var req = rp.defaults({baseUrl: 'http://localhost:3000/', json: true});
        it('should proceed to next handler in chain if request is valid', ()=> {
          return req({url: '/', headers: {val: 'valid'}})
              .then(body=> {
                expect(body).to.deep.equal({reachedHere: true});
              });
        });
        it('should respond with 422 and an error object automatically if request is not valid', ()=> {
          return req({url: '/', headers: {val: 'invalid'}})
              .then(()=> {
                throw new Error('should not get here');
              })
              .catch(reason=> {
                expect(reason.statusCode).to.eq(422);
                expect(reason.error).to.deep.equal({val: 'invalid'});
              });
        });
        it('should be able to use the "this" arg to refer to the context in the validation rule function', ()=> {
          return req({url: '/this_arg', headers: {val1: 'val', val2: 'val'}})
              .then(()=> {
                throw new Error('should not get here');
              })
              .catch(reason=> {
                expect(reason.statusCode).to.eq(422);
                expect(reason.error).to.deep.equal({val2: 'err'});
              });
        });
      });
    });
  });
});